const request = require('request');
const sha512 = require('js-sha512');
const TokenGenerator = require('uuid-token-generator');
const tokenGen = new TokenGenerator();

const pgp = require('pg-promise')();
const db = pgp({
  host: 'db',
  port: 5432,
  database: process.argv[4],
  user: process.argv[2],
  password: process.argv[3],
  max: 30 // use up to 30 connections
});

const express = require('express');
const app = express();
const port = 3000;
const cors = require('cors');
if (process.argv[5] === 'false')
  app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

function getDateWithTimeZone(addMinutes = 0) {
  let nowTimpStamp = (new Date()).getTime() + (7 * 60 * 60 * 1000);
  if (addMinutes > 0)
    nowTimpStamp += (addMinutes * 60 * 1000);
  return new Date(nowTimpStamp);
}

app.post('/api/login', async (req, res) => {
  let body = req.body;
  let username = body.username;
  let password = body.password;
  if (username == null || password == null) {
    res.status(400).end();
    return;
  }
  let passHash = sha512(username + password + username);

  let user = null;
  try {
    user = await db.one("SELECT * from users where username = $1 and status = 'Y'", [username]);
  } catch (e) {
    res.send({resultSuccess: false, errorMsg: 'Incorrect username/password {1}'});  //FIXME create user and create_date
    return;
  }

  let token = tokenGen.generate();
  if (user.password_hash == null || user.password_hash.trim().length == 0) {
    await db.none("update users set token = $1, token_expired = $3, last_login = $4 where username = $2 and status = 'Y'"
      , [token, username, getDateWithTimeZone(10), getDateWithTimeZone()]
    )
    ;
    res.send({
      resultSuccess: true,
      token: token,
      errorMsg: 'You are first logging in, will going to change password'
    });
    return;
  }

  if (user.password_hash !== passHash) {
    res.send({resultSuccess: false, errorMsg: 'Incorrect username/password {2}'});
    return;
  }

  await db.none("update users set token = $1, token_expired = $3, last_login = $4 where username = $2 and status = 'Y'"
    , [token, username, getDateWithTimeZone(10), getDateWithTimeZone()]
  )
  ;
  res.send({resultSuccess: true, errorMsg: null, token: token});
});

app.post('/api/validateToken', async (req, res) => {
  let body = req.body;
  let token = body.token;

  if (token == null) {
    res.status(400).end();
    return;
  }

  let user = null;
  try {
    user = await db.one("SELECT username, report_to from users where token = $1 and token_expired > $2 and status = 'Y'"
      , [token, getDateWithTimeZone()]);
  } catch (e) {
    res.send({resultSuccess: false});
    return;
  }

  res.send({resultSuccess: true, user: user});
});

app.post('/api/changePassword', async (req, res) => {
  let body = req.body;
  let token = body.token;
  let newPassword = body.newPassword;

  if (token == null || newPassword == null) {
    res.status(400).end();
    return;
  }

  let user;
  try {
    user = await db.one("SELECT username, report_to from users where token = $1 and token_expired > $2 and status = 'Y'"
      , [token, getDateWithTimeZone()]);
  } catch (e) {
    res.send({resultSuccess: false, errorMsg: 'Invalid token'});
    return;
  }

  await db.none("update users set password_hash = $1 where token = $2 and token_expired > $3 and status = 'Y'"
    , [sha512(user.username + newPassword + user.username), token, getDateWithTimeZone()]);


  res.send({resultSuccess: true, errorMsg: null});
});

app.get('/api/getDailySubmit', async (req, res) => {
  let body = req.query;

  try {
    let dailySubmitForm = await db.one('select * from daily_submit where username = $1 and submit_date = $2 '
      , [body.username, getDateWithTimeZone()]);
    res.send({resultSuccess: true, dailySubmitForm: dailySubmitForm});
  } catch (e) {
    res.send({resultSuccess: false});
  }
});

app.post('/api/saveDailySubmit', async (req, res) => {
  let body = req.body;

  try {
    await db.none('insert into daily_submit(username, submit_date, location, symptom, others, create_date, update_date)' +
      'values($1, $5, $2, $3, $4, $5, $5)'
      , [body.username, body.location, body.symptom, body.others, getDateWithTimeZone()]);
  } catch (e) {
    await db.none('update daily_submit set location = $1, symptom = $2, others = $3, update_date = $5 ' +
      'where username = $4 and submit_date = $5', [body.location, body.symptom, body.others, body.username, getDateWithTimeZone()])
  }

  res.send({resultSuccess: true});
});

app.get('/api/getReporters', async (req, res) => {
  let reporters = await db.manyOrNone('select distinct report_to, tier from users where report_to is not null order by tier, report_to');
  res.send(reporters);
});

app.get('/api/getTeams', async (req, res) => {
  let teams = await db.manyOrNone('select distinct team from users where team is not null order by team');
  res.send(teams);
});

app.get('/api/getDailyReport', async (req, res) => {
  let body = req.query;
  let reporter = body.reporter;
  let team = body.team;
  let reportDate = new Date(parseInt(body.reportDate) + (7 * 60 * 60 * 1000));

  let sql = 'WITH RECURSIVE user_tree AS (\n' +
    '    SELECT u.username\n' +
    '         , u.report_to\n' +
    '         , u.team\n' +
    '         , ds.location\n' +
    '         , ds.symptom\n' +
    '         , ds.others\n' +
    '         , ds.update_date\n' +
    '    FROM users u\n' +
    '             left join daily_submit ds on u.username = ds.username and ds.submit_date = $2\n' +
    '    WHERE u.username = $1\n' +
    "      and u.status = 'Y'\n" +
    '    UNION ALL\n' +
    '    SELECT users.username\n' +
    '         , users.report_to\n' +
    '         , users.team\n' +
    '         , d.location\n' +
    '         , d.symptom\n' +
    '         , d.others\n' +
    '         , d.update_date\n' +
    '    FROM users\n' +
    '             inner join user_tree on users.report_to = user_tree.username\n' +
    '             left join daily_submit d on users.username = d.username and d.submit_date = $2\n' +
    "    WHERE users.status = 'Y'\n" +
    ')\n' +
    'select *\n' +
    'from user_tree\n';

  if (team !== '')
    sql += 'where team is null or team = $3\n'

  sql += 'order by report_to nulls first, username;';

  let reports = await db.manyOrNone(sql, [reporter, reportDate, team]);
  res.send(reports);
});

app.get('/api/getSummaryDailyReport', async (req, res) => {
  let sql = 'SELECT u.team\n' +
    '     , count(ds.location) submited\n' +
    '     , count(u.username)  total\n' +
    'FROM users u\n' +
    '         left join daily_submit ds on u.username = ds.username and ds.submit_date = $1\n' +
    "where u.status = 'Y'\n" +
    'group by u.team\n' +
    'order by u.team';

  let currentDate = getDateWithTimeZone();

  let reports = await db.manyOrNone(sql, [currentDate]);
  res.send(reports);
});

app.get('/api/getCountSiteCpac', async (req,res) =>{
  let sql = 'select count(1)\n' +
    'from daily_submit\n' +
    'where submit_date = $1\n' +
    '  and location = \'SCG_CPAC_BANGSON\'';

  let currentDate = getDateWithTimeZone();

  let reports = await db.one(sql, [currentDate]);
  res.send(reports);
});

app.get('/api', (req, res) => res.send('Hello API! '));
app.get('/', (req, res) => res.send('Hello World!! '));

app.listen(port, () => console.log(`Example app listening on port ${port}!`));


var options = {
  method: 'POST',
  url: 'https://myaccess.scg.co.th/j_security_check',
  headers: {
    'postman-token': '14bb1081-a7ef-cf15-bb0a-674a17b02463',
    'cache-control': 'no-cache',
    'content-type': 'application/x-www-form-urlencoded'
  },
  form: {
    j_username: 'chaiphet',
    j_password: '1234',
    domainName: 'itoneco.com',
    AUTHRULE_NAME: 'ADAuthenticator',
    domainAuthen: 'true'
  }
};

// request(options, function (error, response, body) {
//   if (error) throw new Error(error);
//
//   console.log(body);
// });
