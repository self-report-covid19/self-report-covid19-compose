const request = require('request');

async function getSummaryDailyReport() {
  await request.get('http://206.189.152.63/api/getSummaryDailyReport',
    (err, res, body) => {
      if (err) return;
      body = JSON.parse(body);
      //console.log(body);
      let msg = 'Self report daily status by team\n';
      for (let team of body) {
        msg += team.submited + ' / ' + team.total + ' - ' + (team.team == null ? 'HEAD' : team.team) + '\n';
      }
      msg += '\nhttp://206.189.152.63/';
      console.log(new Date(), msg);
      sendNotify(msg, 'PRD');
    });
}

const chaiphetToken = 'IaU4SMxocIZsp2y28MTfevi8GhFoQ5Evf5qBOlBSklF';
const itoneCpacToken = 'GJwCVYPjDMwA1gHfetvzb8Ivf890MA89Jd9Tx8uDooi';

async function sendNotify(message, env = 'DEV') {
  await request.post('https://notify-api.line.me/api/notify', {
    //proxy: 'http://cpac_devops:P%40ssw0rd%231@10.0.194.2:3128',
    // auth: {
    //   bearer: env === 'PRD' ? chaiphetToken : 'itoneCpacToken'
    // },
    headers: {
      authorization: 'Bearer ' + (env === 'PRD' ? itoneCpacToken : chaiphetToken)
    }
    , form: {message: '\n\n' + message}
  });
}

getSummaryDailyReport();
