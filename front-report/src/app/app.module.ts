import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {DailySubmitComponent} from './daily-submit/daily-submit.component';
import {FormsModule} from '@angular/forms';
import {ChangePasswordComponent} from './change-password/change-password.component';
import { DailyReportComponent } from './daily-report/daily-report.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DailySubmitComponent,
    ChangePasswordComponent,
    DailyReportComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
