import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-daily-report',
  templateUrl: './daily-report.component.html',
  styleUrls: ['./daily-report.component.css']
})
export class DailyReportComponent implements OnInit {

  constructor(
    private http: HttpClient
  ) {
  }

  dailyReportForm = {
    reporter: '',
    team: '',
    reportDate: {year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate()}
  };

  reporters = [];
  teams = [];
  reportResult = [];
  summarize = '';
  countSiteCpac = '';

  ngOnInit(): void {
    this.http.get<any>(environment.serviceUrl + '/getReporters').subscribe(data => {
        this.reporters = data;
        this.dailyReportForm.reporter = data[0].report_to;
      }
    );

    this.http.get<any>(environment.serviceUrl + '/getTeams').subscribe(data => {
        this.teams = data;
      }
    );
  }

  search() {
    const reporter = this.dailyReportForm.reporter;
    const team = this.dailyReportForm.team;
    const reportDateModel = this.dailyReportForm.reportDate;
    const reportDate = new Date(reportDateModel.year, reportDateModel.month - 1, reportDateModel.day).getTime();

    this.http.get<any>(environment.serviceUrl + '/getDailyReport?reporter=' + reporter + '&team=' + team + '&reportDate=' + reportDate).subscribe(data => {
      this.reportResult = data;

      let reported = 0;
      for (const item of this.reportResult) {
        if (item.update_date) {
          reported++;
          item.update_date = new Date(new Date(item.update_date).getTime() - (7 * 60 * 60 * 1000));
        }
      }
      this.summarize = 'Summarize ' + reported + '/' + data.length;
    });

    this.http.get<any>(environment.serviceUrl + '/getCountSiteCpac').subscribe(data => {
      this.countSiteCpac = 'On CPAC Site : ' + data.count;
    });
  }

}
