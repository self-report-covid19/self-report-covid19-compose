import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Router} from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  changePasswordForm = {
    newPassword: null,
    repeatPassword: null
  };

  passwordInvalid = false;
  errorMsg = null;

  ngOnInit(): void {
  }

  changePassword() {
    if (this.changePasswordForm.newPassword !== this.changePasswordForm.repeatPassword) {
      this.passwordInvalid = true;
      this.errorMsg = 'Repeat password did not match';
      return;
    }

    this.passwordInvalid = false;
    this.http.post<any>(environment.serviceUrl + '/changePassword', {
      token: localStorage.getItem('token'),
      newPassword: this.changePasswordForm.newPassword
    }).subscribe(data => {
      if (data.errorMsg) {
        this.passwordInvalid = true;
        this.errorMsg = data.errorMsg;
        return;
      }
      this.passwordInvalid = false;

      localStorage.clear();
      alert('Change password successfully, please login again.');
      location.href = '/';
    });
  }

}
