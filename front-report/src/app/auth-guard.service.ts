import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, Subject} from 'rxjs';
import {environment} from '../environments/environment';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private http: HttpClient,
  ) {
  }

  subject = new Subject<boolean>();

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const token = localStorage.getItem('token');
    if (token == null) {
      // location.href = '/login';
      this.router.navigate(['login']);
      return false;
    }
    return this.http.post<any>(environment.serviceUrl + '/validateToken', {
      token: token
    }).pipe(map(res => {
      if (!res.resultSuccess) {
        localStorage.removeItem('token');
        localStorage.removeItem('user');
        this.router.navigate(['login']);
      } else {
        localStorage.setItem('user', JSON.stringify(res.user));
      }
      this.subject.next(res.resultSuccess);
      return res.resultSuccess;
    }));
  }

}
