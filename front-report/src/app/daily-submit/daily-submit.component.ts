import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Component({
  selector: 'app-daily-submit',
  templateUrl: './daily-submit.component.html',
  styleUrls: ['./daily-submit.component.css']
})
export class DailySubmitComponent implements OnInit {

  constructor(
    private http: HttpClient
  ) {
  }

  username;
  dailySubmitForm = {
    username: null,
    submit_date: null,
    location: '',
    symptom: '',
    others: null,
    update_date: null
  };
  isSubmitted = false;

  ngOnInit(): void {
    this.username = JSON.parse(localStorage.getItem('user'))['username'];
    this.http.get<any>(environment.serviceUrl + '/getDailySubmit?username=' + this.username).subscribe(data => {
      if (data.resultSuccess) {
        this.isSubmitted = true;
        this.dailySubmitForm = data.dailySubmitForm;
        this.dailySubmitForm.update_date = new Date(new Date(data.dailySubmitForm.update_date).getTime() - (7 * 60 * 60 * 1000));
      } else {
        this.isSubmitted = false;
        this.dailySubmitForm.username = this.username;
        this.dailySubmitForm.submit_date = new Date();
      }
    });
  }

  submitDailyForm() {
    this.http.post(environment.serviceUrl + '/saveDailySubmit', this.dailySubmitForm).subscribe(data => {
      this.ngOnInit();
    });
  }

}
