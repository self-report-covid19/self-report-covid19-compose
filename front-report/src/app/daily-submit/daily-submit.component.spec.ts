import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DailySubmitComponent} from './daily-submit.component';

describe('DailySubmitComponent', () => {
  let component: DailySubmitComponent;
  let fixture: ComponentFixture<DailySubmitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DailySubmitComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailySubmitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
