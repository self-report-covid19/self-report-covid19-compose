import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {DailySubmitComponent} from './daily-submit/daily-submit.component';
import {AuthGuard} from './auth-guard.service';
import {ChangePasswordComponent} from './change-password/change-password.component';
import {DailyReportComponent} from './daily-report/daily-report.component';


const routes: Routes = [
  {path: '', redirectTo: 'daily-submit', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'change-password', component: ChangePasswordComponent, canActivate: [AuthGuard]},
  {path: 'daily-submit', component: DailySubmitComponent, canActivate: [AuthGuard]},
  {path: 'daily-report', component: DailyReportComponent, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
