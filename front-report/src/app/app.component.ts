import {Component, OnInit} from '@angular/core';
import {AuthGuard} from './auth-guard.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(
    private authGuard: AuthGuard,
  ) {
  }

  isLoggedIn = false;
  username = '';

  ngOnInit(): void {
    this.authGuard.subject.subscribe(loggedIn => {
      this.isLoggedIn = loggedIn;
      if (loggedIn) {
        this.username = JSON.parse(localStorage.getItem('user')).username;
      }
    });
  }

  logout() {
    location.href = '/';
    localStorage.clear();
  }

}
