import {Component, OnInit} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
  }

  userForm: any = {};
  errorMsg = null;

  ngOnInit(): void {
  }

  login() {
    this.http.post<any>(environment.serviceUrl + '/login', {
      username: this.userForm.username,
      password: this.userForm.password
    }).subscribe(data => {
      if (!data.resultSuccess) {
        localStorage.removeItem('token');
        this.errorMsg = data.errorMsg;
        return;
      }

      this.errorMsg = null;
      localStorage.setItem('token', data.token);

      if (data.errorMsg) {
        alert(data.errorMsg);
        this.router.navigate(['change-password']);
      } else {
        location.href = '/';
      }
    });
  }

}
