create table users
(
    username      varchar(50) not null,
    tier          integer,
    report_to     varchar(50),
    token_expired timestamp,
    token         varchar(130),
    password_hash varchar(130),
    create_date   timestamp,
    last_login    timestamp,
    team          varchar(20),
    status        varchar(1) default 'Y'::character varying,
    constraint users_pk
        primary key (username)
);

alter table users
    owner to root;

create table daily_submit
(
    username    varchar(50),
    submit_date date,
    location    varchar(50) not null,
    symptom     varchar(50) not null,
    others      varchar(50),
    create_date timestamp,
    update_date timestamp,
    constraint daily_submit_pk
        unique (username, submit_date)
);

alter table daily_submit
    owner to root;


